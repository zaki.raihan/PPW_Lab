from django.contrib import admin

# Register your models here.
from .models import Pengguna, MovieKu, SudahDitonton
admin.site.register(Pengguna)
admin.site.register(MovieKu)
admin.site.register(SudahDitonton)